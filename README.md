# Temperature measurement extension

An extension to use the Temper USB digital thermometers.

Requires [`temper-py`](https://github.com/urwen/temper):

`ofm activate`  
`pip install temper-py`
