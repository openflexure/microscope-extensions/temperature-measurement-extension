import logging
import temper
from labthings import fields, find_component
from labthings.extensions import BaseExtension
from labthings.views import ActionView, PropertyView
from openflexure_microscope.api.utilities.gui import build_gui

def get_temperature():
    temper_device = temper.Temper()
    temper_device_info = temper_device.read()
    
    return temper_device_info[0].get('internal temperature')


def log_temperature(logging_interval, logging_length):
    return 0


class GetTemperature(ActionView):
    """
    Get the temperature from a temper device
    """
    schema = {"temperature": fields.Number(required = True)}
    def post(self):
        temperature = get_temperature()
        logging.info('Temperature: {}'.format(temperature))
        return temperature
    
class LogTemperature(ActionView):
    """
    Log the temperature from a temper device to file
    """
    args = {
        "logging_interval": fields.Number(
            missing = 1, example =1,description ="The interval between temperature logs (minutes)"
        ),
        "logging_length": fields.Number(
            missing = 1, example = 1, description = "The length of logging temperatures (minutes)"
        )
    }
    def post(self, args):
        logging_interval  = args.get("logging_interval")
        logging_length = args.get("logging_length")

extension_gui = {
    "icon": "thermostat",
    "forms" :[
        {
            "name": "Get temperature",
            "route": "/get-temperature",
            "isTask": False,
            "isCollapsible": False,
            "submitLabel": "Get temperature"
        },
        {
            "name": "Log temperature",
            "route": "/log-temperature",
            "isTask": True,
            "isCollapsible": False,
            "submitLabel": "Start temperature log",
            "schema":[
                {
                    "fieldType": "numberInput",
                    "name": "logging_interval",
                    "label": "Logging interval (mins)",
                    "min" : 0,
                    "step":1,
                    "default":1,
                },
                {
                    "fieldType": "numberInput",
                    "name": "logging_length",
                    "label": "Logging length (mins)",
                    "min" : 0,
                    "step":1,
                    "default":1,
                }

            ]
        }

    ]
}

temperature_measurement = BaseExtension("org.openflexure.temperature_measurement", version = "0.0.0")

temperature_measurement.add_method(get_temperature, "get_temperature")
temperature_measurement.add_method(log_temperature, "log_temperature")

temperature_measurement.add_view(GetTemperature,"/get-temperature")
temperature_measurement.add_view(LogTemperature, "/log-temperature")

temperature_measurement.add_meta("gui", build_gui(extension_gui, temperature_measurement))
